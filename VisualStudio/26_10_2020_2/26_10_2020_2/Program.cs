﻿using System;
using System.Net.Http.Headers;

namespace _26_10_2020_2
{
    class Program
    {
        static void Main(string[] args)
        {
            // Console.WriteLine("Hello World!");
            /*
            string[] lista = new string[4];
            lista[0] = "hola";
            lista[1] = "que";
            lista[2] = "tal";
            lista[3] = "?";

            for (int i = 0; i < lista.Length; i++)
            {
                Console.WriteLine(lista[i]);
            }
            */



            Console.WriteLine("¿Cuantas notas quieres evaluar?");
            int total = Int32.Parse(Console.ReadLine());

            int[] notas = new int[total];
            int contadorAprobados = 0;

            for (int i = 0; i < notas.Length; i++)
            {
                Console.WriteLine("Inserta una nota sin decimales");
                notas[i] = Int32.Parse(Console.ReadLine());
                if (notas[i] >= 5)
                {
                    contadorAprobados++;
                }
            }
            double solucion = contadorAprobados / (double)total * 100;

            Console.WriteLine("El tanto por ciento de aprbados es; " + solucion);


        }
    }
}
