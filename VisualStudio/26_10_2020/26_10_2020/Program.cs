﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace _26_10_2020
{
    class Program
    {
        static void Main(string[] args)
        {
            //  holaNombre("Paloma");

            /*double mediaVariable = media(3.5, 5, 9.2);
            Console.WriteLine(mediaVariable);*/

            //int entero = mayor(5, 9, 125);
            //bool entero = patata(5);

            Console.WriteLine(darLaVuelta("ArribaLaBirra"));
        }
        /*
        public static void holaMundo()
        {
            Console.WriteLine("Hola Mundo");
        }
        

        public static void holaNombre(string nombre)
        {
            Console.WriteLine("Hola " + nombre);
        }
        

        public static double media(double num1, double num2, double num3)
        {
            double solucion = 0;

            solucion = ((num1 + num2 + num3) / 3);

            return solucion;
        }
        

        public static int mayor(int num1, int num2, int num3)
        {

            int solucion = 0;

            solucion = Math.Max(Math.Max(num1, num2), num3);

            return solucion;
        }
        

        public static int menor(int num1, int num2, int num3)
        {

            int solucion = 0;

            solucion = Math.Min(Math.Min(num1, num2), num3);

            return solucion;
        }
        

        public static bool par(int num1)
        {
            bool salida = true;

            if (num1 % 2 == 0)
            {
                salida = true;
            }
            else
            {
                salida = false;
            }

            return salida;
        }

        public static bool impar(int num1)
        {
            bool salida = true;

            salida = !par(num1);

            if (num1 %2 == 0)
            {
                salida = false;
            }
            else
            {
                salida = true;
            }
            return salida;
        }


        public static bool patata(int num1)
        {
            bool salida = true;
            
            int contador;
            contador = 0;

            for (int i = num1; i > 0; i--)
            {
                if (num1 % i == 0)
                {
                    contador++;
                }
            }
            if ( contador == 2 || num1 == 1)
            {
                salida = true;
            }
            else
            {
                salida = false;
            }

            return salida;
        }
            */

        public static string darLaVuelta(string frase)
        {
            string solucion = "";

            for (int i = frase.Length -1; i > 0; i--)
            {
                solucion = solucion + frase.Substring(i, 1);
            }

            return solucion;
        }


    }

   

 
}