﻿using System;

namespace switchcase
{
    class Program
    {
        static void Main(string[] args)
        {
            int n1, n2;
            string operacion;

            n1 = Int32.Parse(Console.ReadLine());
            n2 = Int32.Parse(Console.ReadLine());
            bool dentro = true;

            while (dentro)
            {
                operacion = Console.ReadLine();
                switch (operacion)
                {
                    case "sumar":
                        Console.WriteLine("Voy a sumar" + (n1 + n2));
                        break;
                    case "restar":
                        Console.WriteLine("Voy a restar" + (n1 - n2));
                        break;
                    case "multiplicar":
                        Console.WriteLine("Voy a multiplicar" + (n1 * n2));
                        break;
                    case "dividir":
                        Console.WriteLine("Voy a dividir" + (n1 / n2));
                        break;
                    case "elevar":
                        Console.WriteLine("Voy a elevar" + (Math.Pow(n1, n2)));
                        break;
                    case "raiz cuadrada":
                        Console.WriteLine("Voy a raiz cuadrada" + (Math.Sqrt(n1)));
                        break;
                    default:
                       dentro = false;
                        break;
                }
            }
            
        }
    }
}
