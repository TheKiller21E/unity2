﻿using System;

namespace masswitch
{
    class Program
    {
        static void Main(string[] args)
        {

            int nmayor, nmenor, ntotal, aux;
            ntotal = 1;
            Console.WriteLine("Introduzca un numero");

            aux = Int32.Parse(Console.ReadLine());
            nmayor = aux;
            nmenor = aux;

            while (aux !=0)
            {

                Console.WriteLine("Introduzca un numero");
                aux = Int32.Parse(Console.ReadLine());
                if (aux != 0)
                {

                    nmayor = Math.Max(aux, nmayor);
                    nmenor = Math.Min(aux, nmenor);
                    ntotal++;
                }

            }

            Console.WriteLine("Has introducido " + ntotal + " El menor  es " + nmenor + " Y el mayor es " + nmayor);
        }
    }
}
