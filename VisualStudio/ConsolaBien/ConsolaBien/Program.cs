﻿using System;

namespace ConsolaBien
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime fecha = new DateTime();
            fecha = DateTime.Now;
            Console.WriteLine(fecha.ToString());

            string fechastring = "27/12/2001 12:12:12";
            fecha = DateTime.Parse(fechastring);
            Console.WriteLine(fecha);

            string dia = fecha.Day.ToString();

            string mes = fecha.Month.ToString();

            string año = fecha.Year.ToString();
            string hora = fecha.Hour.ToString();
            string min = fecha.Minute.ToString();
            string seg = fecha.Second.ToString();

            Console.WriteLine("Hoy es " + dia + " del mes " + mes + " del año " + año + " a la hora " + hora + " en el minuto " + min + " en el segundo " + seg );
        }
    }
}


