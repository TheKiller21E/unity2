﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvionController : MonoBehaviour
{
    [Tooltip("Bomba que se lanza")]
    public GameObject bombita;
    [Tooltip("Punto donde se generan las bombas")]
    public Transform SpawnPosition;
    // Start is called before the first frame update
    void Start()
    {
        Bombard();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void Bombard()
    {
        if(bombita != null)
        {
            Instantiate(bombita);
        }
    }
}
